module.exports = {
  "extends": "airbnb",
  "env": {
    "browser": true,
    "node": true
  },
  "plugins": [
    "react",
    "jsx-a11y",
    "import"
  ],
  "rules": {
    "max-len": ["error", { "code": 120 }],
    "react/jsx-filename-extension": [2, {
      "extensions": [".js"]
    }],
    "react/jsx-no-bind": [2, {
      "ignoreRefs": false,
      "allowArrowFunctions": false,
      "allowBind": false
    }],
    "react/forbid-prop-types": [2, { "forbid": ["any", "array"] }],
    "jsx-a11y/no-static-element-interactions": [0],
  }
};