import config from '../config';

export const RESET = 'crewBoard/RESET';
export const GET_CREW_DATA = 'crewBoard/GET_CREW_DATA';
export const GET_CREW_DATA_SUCCESS = 'crewBoard/GET_CREW_DATA_SUCCESS';
export const MOVE_CARD = 'crewBoard/MOVE_CARD';

const initialState = {
  members: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case RESET:
      return initialState;

    case GET_CREW_DATA_SUCCESS: {
      const members = action.payload;
      const membersWithAssignedColumns = members.map(member => ({
        ...member,
        columnName: 'Applied',
      }));

      return {
        ...state,
        members: membersWithAssignedColumns,
      };
    }

    case MOVE_CARD: {
      const members = [...state.members];
      const cardId = action.payload.cardId;
      const cardIndex = members.indexOf(members.find(member => member.id.value === cardId));
      members[cardIndex].columnName = action.payload.columnName;

      return {
        ...state,
        members,
      };
    }

    default:
      return state;
  }
};


export const getUsersData = () => (dispatch) => {
  dispatch({
    type: GET_CREW_DATA,
  });

  const fetchParams = {
    method: 'GET',
  };

  fetch(config.crewPersonalApiUrl, fetchParams).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw new Error();
  }).then((json) => {
    dispatch({
      type: GET_CREW_DATA_SUCCESS,
      payload: json.results,
    });
  });
};

export const moveCardToColumn = (cardId, columnName) => (dispatch) => {
  dispatch({
    type: MOVE_CARD,
    payload: {
      cardId,
      columnName,
    },
  });
};
