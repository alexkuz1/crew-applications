import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import CrewCard from './components/CrewCard';

import './CrewBoard.css';
import './components/Components.css';
import { getUsersData } from './crewBoardReducer';

const errorText = 'No crew applications were found.';

class CrewBoard extends Component {

  static renderCards(associatedCards, moveLeftTo, moveRightTo) {
    return associatedCards.map(
      cardData =>
        <CrewCard
          key={cardData.id.value}
          data={cardData}
          moveLeftTo={moveLeftTo}
          moveRightTo={moveRightTo}
        />,
    );
  }

  componentDidMount() {
    this.props.getUsersDataDispatch();
  }

  renderColumn({ columnName, moveLeftTo, moveRightTo }) {
    const { members } = this.props;
    const associatedCards = members.filter(member => member.columnName === columnName);
    return (
      <div className="board__column">
        <span className="board__title">{columnName}</span>
        {CrewBoard.renderCards(associatedCards, moveLeftTo, moveRightTo)}
      </div>
    );
  }

  render() {
    const { members } = this.props;
    if (members.length === 0) {
      return (
        <div className="board__wrapper">
          <div className="board__error-text">
            <span>{errorText}</span>
          </div>
        </div>
      );
    }
    return (
      <div className="board__wrapper">
        {this.renderColumn({ columnName: 'Applied', moveRightTo: 'Interviewing' })}
        {this.renderColumn({ columnName: 'Interviewing', moveLeftTo: 'Applied', moveRightTo: 'Hired' })}
        {this.renderColumn({ columnName: 'Hired', moveLeftTo: 'Interviewing' })}
      </div>
    );
  }
}

CrewBoard.propTypes = {
  members: PropTypes.arrayOf(PropTypes.object).isRequired,
  getUsersDataDispatch: PropTypes.func.isRequired,
};

export default connect(
  state => ({
    members: state.crewBoard.members,
  }), {
    getUsersDataDispatch: getUsersData,
  })(CrewBoard);
