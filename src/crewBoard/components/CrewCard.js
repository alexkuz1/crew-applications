import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { moveCardToColumn } from '../crewBoardReducer';

class CrewCard extends Component {

  constructor(props) {
    super(props);
    this.moveCardToColumnLeft = this.moveCardToColumnLeft.bind(this);
    this.moveCardToColumnRight = this.moveCardToColumnRight.bind(this);
  }

  moveCardToColumnLeft() {
    const { moveLeftTo, moveCardToColumnDispatch, data: { id } } = this.props;
    moveCardToColumnDispatch(id.value, moveLeftTo);
  }

  moveCardToColumnRight() {
    const { moveRightTo, moveCardToColumnDispatch, data: { id } } = this.props;
    moveCardToColumnDispatch(id.value, moveRightTo);
  }

  render() {
    const { email, name, picture } = this.props.data;
    const { moveLeftTo, moveRightTo } = this.props;
    const firstName = name.first[0].toUpperCase() + name.first.substring(1);
    const lastName = name.last[0].toUpperCase() + name.last.substring(1);
    return (
      <div className="board-card__wrapper">
        <img className="board-card__avatar" alt="user-avatar" src={picture.thumbnail} />
        <div className="board-card__userinfo-text">
          <span>{`${firstName} ${lastName}`}</span>
          <span>{email}</span>
        </div>
        <div className="board-card__actions">
          { moveLeftTo ? <span onClick={this.moveCardToColumnLeft}>{'<- '}</span> : null }
          { moveRightTo ? <span onClick={this.moveCardToColumnRight}>{' ->'}</span> : null }
        </div>
      </div>
    );
  }
}

CrewCard.defaultProps = {
  moveLeftTo: '',
  moveRightTo: '',
};

CrewCard.propTypes = {
  data: PropTypes.object.isRequired,
  moveLeftTo: PropTypes.string,
  moveRightTo: PropTypes.string,
  moveCardToColumnDispatch: PropTypes.func.isRequired,
};

export default connect(() => ({}), {
  moveCardToColumnDispatch: moveCardToColumn,
})(CrewCard);
