import { combineReducers } from 'redux';
import crewBoardReducer from './crewBoard/crewBoardReducer';

export default combineReducers({
  crewBoard: crewBoardReducer,
});
